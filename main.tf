provider "aws" {
  region = "eu-central-1"
}

variable "cidr_vpc" {}
variable "cidr_subnet_priv" {}
variable "cidr_subnet_pub" {}

variable "key" {
  description = "key to access the instance"
  default     = "tfhele"
  type        = string
}

resource "aws_vpc" "hele-vpc" {
  #  cidr_block = "10.0.0.0/16"
  cidr_block = var.cidr_vpc
  tags = {
    Name = "HeleVPC"
  }
}

resource "aws_subnet" "hele-subnet-pub" {
  vpc_id = aws_vpc.hele-vpc.id
  #  cidr_block              = "10.0.10.0/24"
  cidr_block              = var.cidr_subnet_pub
  map_public_ip_on_launch = "true"
  tags = {
    Name = "hele-subnet-pub"
  }
}

resource "aws_subnet" "hele-subnet-priv" {
  vpc_id = aws_vpc.hele-vpc.id
  #  cidr_block = "10.0.20.0/24"
  cidr_block = var.cidr_subnet_priv
  tags = {
    Name = "hele-subnet-priv"
  }
}

##IGW provisioning

resource "aws_internet_gateway" "hele-igw" {
  vpc_id = aws_vpc.hele-vpc.id
  tags = {
    Name = "hele-subnet-pub-rtb"
  }

}

resource "aws_default_route_table" "hele-pub-main-rtb" {
  default_route_table_id = aws_vpc.hele-vpc.default_route_table_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.hele-igw.id
  }
  tags = {
    Name = "hele-pub-main-rtb"
  }
}

##NatGW provisioning

resource "aws_eip" "hele-eip-natgw" {
  vpc = true
}

resource "aws_nat_gateway" "hele-natgw" {
  allocation_id = aws_eip.hele-eip-natgw.id
  subnet_id     = aws_subnet.hele-subnet-pub.id

  tags = {
    Name = "hele-natgw"
  }
  depends_on = [aws_internet_gateway.hele-igw]
}

resource "aws_route_table" "private-route-table" {
  vpc_id = aws_vpc.hele-vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.hele-natgw.id
  }
  tags = {
    Name = "hele-subnet-priv-rtb"
  }

}

resource "aws_route_table_association" "rtb-to-hele-priv-subnet" {
  subnet_id      = aws_subnet.hele-subnet-priv.id
  route_table_id = aws_route_table.private-route-table.id
}

##Provisioning EC2 instance

data "aws_ami" "amazon-linux2-image" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }
}

resource "aws_instance" "al2" {
  ami           = data.aws_ami.amazon-linux2-image.image_id #"ami-00f22f6155d6d92c5"
  instance_type = "t2.micro"
  #  key_name      = "tfhele"
  key_name  = var.key
  subnet_id = aws_subnet.hele-subnet-priv.id
  user_data = file("update.sh")

  tags = {
    Name = "terraformed-instance"
  }
}

